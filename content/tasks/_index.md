+++
title = "Задания"
weight = 3
anchor = "tasks"
+++

##### Южная Америка

* [Карта природных зон](/tasks/Karta_prirodnyx_zon.jpg)
* [Природные зоны Южной Америки](/tasks/Prirodnye_zony_JUzhnoj_Ameriki.pptx)
* [Таблица для заполнения](/tasks/Tablica_dlja_zapolnenija.docx)

