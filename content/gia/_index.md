+++
title = "Подготовка к ГИА"
weight = 2
anchor = "gia"
+++

* [Россия и пограничные государства](/gia/Rossija_i_pogranichnye_gosudarstva.gif)
* [Россия. Приграничные государства](/gia/Rossija._Prigranichnye_gosudarstva.jpg)
* [Контурная карта России. Тренировочная](/gia/Konturnaja_karta_Rossii._Trenirovochnaja.jpg)
* [Движение населения](/gia/Dvizhenie_naselenija.docx)
* [Координаты](/gia/Koordinaty.docx)
* [Определение направлений по карте](/gia/Opredelenie_napravlenij_po_karte.docx)
* [Определение расстояний по карте](/gia/Opredelenie_rasstojanij_po_karte.docx)
* [Плотность населения регионов России](/gia/Plotnost'_naselenija_regionov_Rossii.docx)
* [Положение России](/gia/Polozhenie_Rossii.docx)
* [Пригодность участков для ведения определённой деятельности](/gia/Prigodnost'_uchastkov_dlja_vedenija_opredeljonnoj_dejatel'nosti.docx)
* [Профиль местности](/gia/Profil'_mestnosti.docx)
* [Работа с информацией. Расчёты](/gia/Rabota_s_informaciej._Raschjoty.docx)
* [Работа с климатограммами](/gia/Rabota_s_klimatogrammami.docx)
* [Тренировочные задания по топографическому плану. 4 варианта по 8 заданий](/gia/Trenirovochnye_zadanija_po_topograficheskomu_planu._4_varianta_po_8_zadanij.docx)


