+++
title = "Карты"
weight = 4
anchor = "maps"
+++

##### География материков и океанов

* [Африка](/maps/Afrika.jpg)
* [Антарктика](/maps/Antarktika.jpg)
* [Арктика](/maps/Arktika.png)
* [Австралия](/maps/Avstralija.jpg)
* [Азия](/maps/Azija.jpg)
* [Европа](/maps/Evropa.jpg)
* [Южная Америка](/maps/JUzhnaja_Amerika.jpg)
* [Северная Америка](/maps/Severnaja_Amerika.jpg)

##### География России

* [Федеральные округа РФ](/maps/Federal'nye_okruga_RF.png)
* [Города-миллионники](/maps/Goroda-millionniki.jpg)
* [Экономические районы России](/maps/JEkonomicheskie_rajony_Rossii.png)
* [Контурная карта России (тренировочная)](/maps/Konturnaja_karta_Rossii._Trenirovochnaja.jpg)
* [Новые часовые зоны](/maps/Novye_chasovye_zony.png)
* [Россия с координатной сеткой](/maps/Rossija_s_koordinatnoj_setkoj.png)
* [Территориально-административное деление РФ](/maps/Territorial'no-administrativnoe_delenie_RF.png)

##### Природа Южной Америки

* [Вариант 1](/maps/JUA._Variant_1.jpg)
* [Вариант 2](/maps/JUA._Variant_2.jpg)
* [Вариант 3](/maps/JUA._Variant_3.jpg)
* [Вариант 4](/maps/JUA._Variant_4.jpg)

##### Природа Северной Америки

* [Вариант 1](/maps/V1.jpg)
* [Вариант 2](/maps/V2.jpg)
* [Вариант 3](/maps/V3.jpg)
* [Вариант 4](/maps/V4.jpg)

##### Природа Евразии

* [Вариант 1](/maps/Variant_1.jpg)
* [Вариант 2](/maps/Variant_2.jpg)
* [Вариант 3](/maps/Variant_3.jpg)
* [Вариант 4](/maps/Variant_4.jpg)

##### Территориально-административное деление РФ

* [Основа](/maps/Osnova.png)
* [Вариант 1](/maps/Variant_1.png)
* [Вариант 2](/maps/Variant_2.png)
* [Вариант 3](/maps/Variant_3.png)

